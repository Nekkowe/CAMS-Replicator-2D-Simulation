from sim import Environment, CAMSReverseAndSidestepAgent, Direction
import datetime
import matplotlib.pyplot as plt
import numpy as np
import os
from pathlib import Path
from matplotlib.animation import FuncAnimation

ANIMATION_OUTPUT_DIRECTORY = "./output"
STEPS = 1000

np.random.seed(123456)
fig, ax = plt.subplots()
env = Environment((100, 100))

start_time = datetime.datetime.now()
start_time_timestamp = start_time.strftime("%Y-%m-%dT%H%M%S%z")
saved_animation_filename = "CAMS-replicator-simulation-{}.mp4".format(start_time_timestamp)
output_path = os.path.join(ANIMATION_OUTPUT_DIRECTORY, saved_animation_filename)

if not os.path.exists(ANIMATION_OUTPUT_DIRECTORY):
    os.makedirs(ANIMATION_OUTPUT_DIRECTORY)

first_agent = CAMSReverseAndSidestepAgent(
    environment=env, 
    position=(50,50), 
    initial_direction=Direction.NORTH,
    required_resources=500
)

im = ax.imshow(env.render(), aspect="equal", origin="lower")

def update(frame_number):
    print(frame_number)
    env.step()
    im.set_data(env.render())
    return im,

ani = FuncAnimation(
    fig=fig, 
    func=update,
    frames=STEPS,
    blit=True,
    interval=1000/60,
    repeat=False
)

#plt.show()

print("Saving animation, this might take some time...")
ani.save(output_path, progress_callback=(lambda i, n: print(f'Saving frame {i}/{n}')))
print("Animation saved.")